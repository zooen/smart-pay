package com.founder.schedule;

import com.founder.core.log.MyLog;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class SyncOrder {

    private final MyLog _log = MyLog.getLog(SyncOrder.class);

    @Scheduled(cron = "0 30 05 * * ?")
    public void run(){
        _log.info("开始同步订单");
    }
}
